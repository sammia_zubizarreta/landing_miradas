#coding:utf-8

from fabric.api import *
from ..config import *


def normalize_password(password):
    return password.replace('$', '\\$')


@task
def dropdb():
    role = env.roles[0]
    for data in DATABASES[role].values():
        run(
            'echo \'DROP DATABASE IF EXISTS %s;\' | mysql -u\'%s\' -p\'%s\''
            % (data['name'],
               data['username'],
               normalize_password(data['password']),),
            shell_escape=False
        )


@task
def createdb():
    role = env.roles[0]
    for data in DATABASES[role].values():
        run(
            'echo \'CREATE DATABASE %s;\' | mysql -u\'%s\' -p\'%s\''
            % (data['name'],
               data['username'],
               normalize_password(data['password'])),
            shell_escape=False
        )


@task
def loaddb(name='default'):
    role = env.roles[0]
    run(
        'mysql -u\'%s\' -p\'%s\' %s < %s/%s'
        % (DATABASES[role][name]['username'],
           normalize_password(DATABASES[role][name]['password']),
           DATABASES[role][name]['name'],
           CLONE_PATH[role], DUMP_FILE),
        shell_escape=False
    )


@task
def dumpdbs_mysql():
    role = env.roles[0]
    for data in DATABASES[role].values():
        run(
            'mysqldump -u\'%s\' -p\'%s\' %s > %s/%s'
            % (data['username'],
               normalize_password(data['password']),
               data['name'],
               env['new_folder'],
               'dump_%s.sql' % data['name']
            ),
            shell_escape=False
        )
