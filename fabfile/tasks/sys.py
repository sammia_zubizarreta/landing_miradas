#coding:utf-8

from fabric.api import *
from ..config import *


@task
def install_dependencies():
    sudo('apt-get install %s' % ' '.join(SYSTEM_DEPENDENCIES))
    sudo('pip install virtualenv')
