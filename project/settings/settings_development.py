# coding:utf-8
import os
from settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'db_miradas',
        'USER': 'root',
        'PASSWORD': 'mysql',
        'HOST': 'localhost',
        'PORT': '',
        'OPTIONS': {
            'init_command': 'SET default_storage_engine=INNODB,'
                            'character_set_connection=utf8,'
                            'collation_connection=utf8_unicode_ci',
            'charset': 'utf8',
            'use_unicode': True,
        }
    }
}

DEBUG = True
BASE_URL = 'http://10.12.101.51:8000'
VIRTUALENV_DIR = os.path.join('environment', 'env-controlgastos')
SETTINGS_FILE = ''

INTERNAL_IPS = ['127.0.0.1']

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    )
}