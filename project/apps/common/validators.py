# coding:utf-8
"""
Validadores de entrada
======================
"""

from django.core.validators import RegexValidator

name_validator = RegexValidator(u'^[a-zA-z \-\'áéíóúÁÉÍÓÚñÑüÜïÏöÖ]+$')
"""
Valida la presencia de caracteres alfabéticos y especiales adecuados para un
nombre.
"""

dni_validator = RegexValidator(u'^[0-9]{8}$')
"""Valida la presencia de 8 caracteres numéricos."""

cell_phone_validator = RegexValidator(u'^[0-9]{9}$', u'Introduzca un teléfono válido.')
"""Valida la presencia de 9 caracteres numéricos."""

phone_validator = RegexValidator(u'^\+?[0-9 -]{7,14}$')
"""
Valida la presencia de 7 a 14 caracteres numéricos con espacios y guiones
(``-``).
"""

alphanumeric_validator = RegexValidator(u'^[ -~áéíóúÁÉÍÓÚñÑüÜ¡¿]+$')
alphanumeric_new_validator = RegexValidator(u'[^ ]?[ -~áéíóúÁÉÍÓÚñÑüÜ¡¿]*[!-~áéíóúÁÉÍÓÚñÑüÜ¡¿]+$')

document_validator = RegexValidator(u'^[0-9A-Za-z]{8,12}$')
"""Valida la presencia de 12 caracteres alphanuméricos."""


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.jpeg', '.png']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Por favor, elija una imagen válida.')