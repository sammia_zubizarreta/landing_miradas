#!encoding:utf-8
from django.conf import settings
from django.template.loader import get_template
from mailer import send_html_mail


def send_mail_html(to, template, title, data={}, headers=None):

    error = True

    if to:
        while error:
            try:
                send_html_mail(
                    subject=title,
                    message=title,
                    message_html=get_template(template).render(data),
                    from_email=settings.EMAIL_FROM_USER,
                    recipient_list=[to],
                    headers=headers
                )
                error = False
            except Exception as e:
                print(e)
                error = True
