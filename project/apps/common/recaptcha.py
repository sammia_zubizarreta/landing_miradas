#!encoding:utf-8
import json
import urllib
from django.conf import settings


class ReCaptcha:

    def __init__(self, token, *args, **kwargs):
        self.token = token
        self.valid = self.validate()

    def validate(self):
        url = settings.RECAPTCHA_URL

        values = {
            'secret': settings.RECAPTCHA_PRIVATE_KEY,
            'response': self.token
        }

        data = urllib.parse.urlencode(values).encode()
        req = urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())

        return result.get('success') and result.get('score', 0.0) > 0.5
