#!encoding:utf-8

# Model Expense
STATUS_EXPENSE = {'ISSUED': u'Emitido', 'APPROVED': u'Aprobó', 'REJECTED': u'Desaprobó', }
STATUS_EXPENSE_REWRITE = {'ISSUED': u'Emitido', 'APPROVED': u'Aprobado', 'REJECTED': u'Desaprobado', }
TYPE_EXPENSE = {'MOBILITY': u'Movilidad', 'ALIMENT': u'Alimento'}
AMOUNT_MAX_EXPENSE = 30

# Managers
APP_FINANCES_NAME = 'FINANCES'
APP_EXPENSES_NAME = 'EXPENSES'
MODEL_EXPENSE_NAME = 'EXPENSE'

# Website
EXPENSE_UPDATE_SUCCESS = u'¡EL GASTO DE {0} SE {1} CORRECTAMENTE!'
EXPENSE_UPDATE_EMPLOYEE = u'{0} puede realizar el cobro de su gasto.'
EXPENSE_UPDATE_EMPLOYEE_SUBTITLE_ONE_APPROVED = u'{0} puede realizar el cobro de su gasto.'
EXPENSE_UPDATE_EMPLOYEE_SUBTITLE_ONE_REJECTED = u'{0} puede realizar el cobro de su gasto.'
EXPENSE_UPDATE_EMPLOYEE_SUBTITLE_TWO = u'{0} puede realizar el cobro de su gasto.'
EXPENSE_UPDATE_ERROR_REWRITE_TITLE = u'¡EL GASTO DE {0} SE ENCUENTRA {1}!'
EXPENSE_UPDATE_ERROR_REWRITE_SUBTITLE = u'Es posible que ya haya realizado esta acción.'
EXPENSE_UPDATE_ERROR_URL = u'MISTAKE IN PARAMETER OF THE SERVICE'
EXPENSE_UPDATE_ERROR_KEY = u'MISTAKE IN PARAMETER OF THE SERVICE'
EXPENSE_AMOUNT_MAX_ERROR = u'Excedió el monto máximo por día'
EXPENSE_AMOUNT_ZERO_ERROR = u'El monto no puede ser cero'
