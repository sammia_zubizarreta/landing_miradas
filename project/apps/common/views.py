# coding:utf-8
import json
from django.http import HttpResponse
from django.views.generic import View


class JSONView(View):
    def dispatch(self, request, *args, **kwargs):
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(),
                              self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed

        data = None

        if request.method == 'POST':
            data = json.loads(request.body)
        response = handler(request, data, *args, **kwargs)
        return HttpResponse(
            content=json.dumps(response),
            content_type='application/json'
        ) if type(response) in (dict, list) else response


class JSONFormView(JSONView):
    form_class = None

    def post(self, request, data, *args, **kwargs):
        form = self.form_class(data=data)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return {'success': False, 'errors': form.errors}

    def form_valid(self, form):
        return {'success': True}


class JSONMultiFormView(JSONView):
    form_list = None

    def post(self, request, data, *args, **kwargs):
        form_list = [form(data=data) for form in self.form_list]
        forms_valid = False not in map(lambda f: f.is_valid(), form_list)
        errors = reduce(lambda a, b: a.update(b) or a,
                        [form.errors for form in form_list])

        if forms_valid:
            return self.form_valid(form_list, data)
        else:
            return {'success': False, 'errors': errors}

    def form_valid(self, forms, data):
        return {'success': True}
