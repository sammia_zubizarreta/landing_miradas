# coding:utf-8
from django.views.generic import TemplateView
from rest_framework import generics
from configurations.models import Participant
from website.serializer import ParticipantSerializer

class HomeView(TemplateView):
    template_name = 'index.html'

class TestView(TemplateView):
    template_name = 'test.html'

class ParticipantAPIView(generics.CreateAPIView):
    queryset = Participant.objects.all()
    serializer_class = ParticipantSerializer

