# coding:utf-8
from django.urls import path, reverse_lazy
from django.views.generic import RedirectView

from website import views

app_name = "website"

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('test', views.TestView.as_view(), name='test'),
    path('api/participant', views.ParticipantAPIView.as_view(), name='participant'),
]