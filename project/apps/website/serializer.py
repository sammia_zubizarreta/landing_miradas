from rest_framework import serializers
from common.recaptcha import ReCaptcha
from configurations.models import Participant

class ParticipantSerializer(serializers.HyperlinkedModelSerializer):

    def validate(self, data):
        token = data['token']
        voucher = data['voucher']

        # if voucher.size > 10174706:
        #     raise serializers.ValidationError(u"La imagen excede el peso máximo de 10 MB.")

        # if not ReCaptcha(token).valid:
        #     raise serializers.ValidationError("El recaptcha es incorrecto.")

        return data

    class Meta:
        model = Participant
        fields = 'name', 'document', 'phone', 'company', 'position', 'email', 'voucher', 'token'