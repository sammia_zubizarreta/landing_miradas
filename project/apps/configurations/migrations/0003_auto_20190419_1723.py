# Generated by Django 2.0.3 on 2019-04-19 22:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('configurations', '0002_auto_20190419_1614'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='participant',
            name='last_name',
        ),
        migrations.AddField(
            model_name='participant',
            name='company',
            field=models.CharField(default=0, max_length=50, verbose_name='Empresa'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='document',
            field=models.CharField(default=0, max_length=20, verbose_name='Documento'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='email',
            field=models.CharField(default=0, max_length=50, verbose_name='Correo Electrónico '),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='phone',
            field=models.CharField(default=0, max_length=9, verbose_name='Teléfono'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='position',
            field=models.CharField(default=1, max_length=50, verbose_name='Cargo'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='participant',
            name='voucher',
            field=models.FileField(max_length=80, upload_to='voucher/', verbose_name='Comprobante'),
        ),
    ]
