#!encoding:utf-8
from __future__ import unicode_literals
import sys
from io import StringIO, BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.validators import validate_email
from django.db import models
from PIL import Image
from common.validators import cell_phone_validator, validate_file_extension


class Participant(models.Model):

    name = models.CharField(
        verbose_name=u'Nombre',
        max_length=50,
        blank=False
    )

    document = models.CharField(
        verbose_name=u'Documento',
        max_length=20,
        blank=False
    )

    phone = models.CharField(
        verbose_name=u'Teléfono',
        max_length=9,
        blank=False,
        validators=[cell_phone_validator]
    )

    company = models.CharField(
        verbose_name=u'Empresa',
        max_length=50,
        blank=False
    )

    position = models.CharField(
        verbose_name=u'Cargo',
        max_length=50,
        blank=False
    )

    email = models.CharField(
        verbose_name=u'Correo Electrónico ',
        max_length=50,
        blank=False,
        validators=[validate_email]
    )

    voucher = models.ImageField(
        verbose_name=u'Comprobante',
        upload_to='voucher/',
        max_length=80,
        # validators=[validate_file_extension]
    )

    token = models.CharField(
        verbose_name=u'Token',
        blank=False,
        max_length=500
    )

    created_at = models.DateTimeField(
        verbose_name=u'Fecha de Creación',
        auto_now_add=True
    )

    def save(self, *args, **kwargs):
        if self.voucher:
            img = Image.open(BytesIO(self.voucher.read()))
            if img.mode != 'RGB':
                img = img.convert('RGB')
            img.thumbnail((self.voucher.width/2, self.voucher.height/2), Image.ANTIALIAS)
            output = BytesIO()
            img.save(output, format='JPEG', quality=70)
            output.seek(0)
            self.voucher = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.voucher.name.split('.')[0],
                                              'image/jpeg', len(output.getbuffer()) , None)
        super(Participant, self).save(*args, **kwargs)

    def __str__(self):
        return u'{0}'.format(self.name)

    class Meta:
        verbose_name = u'Participante'
        verbose_name_plural = u'Participantes'
        ordering = ('-created_at',)