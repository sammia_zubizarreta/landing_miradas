#!encoding:utf-8
from django.apps import AppConfig

class ConfigurationsConfig(AppConfig):
    name = 'configurations'
    verbose_name = u'Configuraciones'