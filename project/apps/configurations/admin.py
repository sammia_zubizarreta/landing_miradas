#!encoding:utf-8
from django.conf import settings
from django.contrib import admin

from common.utils import ExcelResponse
from configurations.models import  Participant


class ParticipantAdmin(admin.ModelAdmin):

    list_display = ('name', 'document', 'phone', 'company', 'position', 'email', 'voucher', 'created_at')
    list_filter = ('created_at',)
    actions = ['export']
    list_per_page = 10

    def export(self, request, queryset):
        header_list = [(u'Nombre', u'Documento', u'Teléfono', u'Empresa', u'Cargo', u'Correo Electrónico', u'Comprobante', u'Fecha de Creación')]

        data_list = []

        list_classification = queryset.values('name', 'document', 'phone', 'company', 'position', 'email', 'voucher', 'created_at')

        for attribute in list_classification:
            row = (
                attribute.get('name'),
                attribute.get('document'),
                attribute.get('phone'),
                attribute.get('company'),
                attribute.get('position'),
                attribute.get('email'),
                u'{0}media/{1}'.format(settings.BASE_URL, attribute.get('voucher')),
                attribute.get('created_at')
            )
            data_list.append(row)
        return ExcelResponse(data=header_list + data_list, output_name='Lista de Participantes')
    export.short_description = u"Descargar Participante/s seleccionado/s"

admin.site.register(Participant, ParticipantAdmin)