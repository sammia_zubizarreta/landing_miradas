var isMobile;
var csrftoken;

$(function(){
  init();
});

function init() {
  initCheckDevice();
  initSlick();
  initJcf();
  initNavegation();
  initFixHeader();
  validateForm();
  saveForm();
}
function initSlick() {
  $('.slick-carousel').slick({
    arrows: true,
    dots: false,
    slidesToShow: 3,
    infinite: true,
    prevArrow: "<img src='static/images/arrow_left.png' class='arrow_left'>",
    nextArrow: "<img src='static/images/arrow_right.png' class='arrow_right'>",
    responsive: [
      {
        breakpoint: 960,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 641,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
}

function initJcf() {
    jcf.replaceAll ();
}

function initNavegation() {
  $('.home').click(function(){
    $('html,body').animate({scrollTop:'0px'}, 1000);return false;
  });

  $('.programa').click(function(){
    if(!isMobile.any() ){
      $('html,body').animate({scrollTop:'610px'}, 1000);return false;
    }
    else{
      $('html,body').animate({scrollTop:'900px'}, 1000);return false;
    }
  });
  $('.ponentes').click(function(){
    if(!isMobile.any() ){
      $('html,body').animate({scrollTop:'2750px'}, 1000);return false;
    }
    else{
      $('html,body').animate({scrollTop:'4530px'}, 1000);return false;
    }
  });
  $('.inscribete').click(function(){
    if(!isMobile.any() ){
      $('html,body').animate({scrollTop:'3200px'}, 1000);return false;
    }
    else{
      $('html,body').animate({scrollTop:'4900px'}, 1000);return false;
    }
  });
}

function initCheckDevice() {
  isMobile = {
      Android: function() {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function() {
          return navigator.userAgent.match(/IEMobile/i);
      },
      any: function() {
          return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
      }
  };
}

function initFixHeader() {
  $(window).scroll(function(){
    var window_y = $(window).scrollTop();
    var scroll_critical = parseInt($("#header-menu").height());
    if(!isMobile.any() ){
      if (window_y > scroll_critical) {
        $("#header-menu").addClass("menuJS");
        $("header nav").addClass("navJS");
        $("main img").addClass("menu_top");
        $(".header__redes").css("display","none");
      } else if(window_y <= scroll_critical) {
        $("#header-menu").removeClass("menuJS");
        $("header nav").removeClass("navJS");
        $("main img").removeClass("menu_top");
        $(".header__redes").css("display","flex");
      }
    }
  });
}

function validateForm() {

  $("#name, #position").alpha({
      disallow: '&,;._-!|"·#@$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…'
  });

  $("#company, #document").alphanum({
    disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
  });


   $('#phone').numeric({
    allowPlus: false,
    allowMinus: false,
    allowDecSep: false,
    allowThouSep: false,
    allowNumeric: true,
    min: 0
  });

  $("#email").alphanum({
      disallow: '&,;!|"·#$%&/()=¡?¿<>+*^`[]{}çœæ®†¥øπå∫∂ƒ™¶§Ω∑©√ßµ„…',
      allow: '@._-'
  });

  var form = $('#participant');
  $(form).validate({
    debug: false,
	errorClass: 'form-error',
	errorElement: 'em',
	ignore: ".select-dropdown",
    rules: {
      name: {
        required: true,
        minlength: 2,
        maxlength: 90
      },
      document: {
        required: true,
        minlength: 8,
        maxlength: 16
      },
      email: {
        required: true,
        minlength: 2,
        maxlength: 60
      },
      phone: {
        required: true,
        minlength: 6,
        maxlength: 9
      },
      company:{
        required: true,
        minlength: 2,
        maxlength: 60
      },
      position:{
        required: true,
        minlength: 2,
        maxlength: 80
      },
      voucher: {
          required:true,
      }
    },
    messages: {
      name: {
        required: 'Ingresa un nombre(s)'
      },
      document: {
        required: 'Ingrese DNI'
      },
      email: {
        required: 'Ingresa un correo'
      },
      phone: {
        required: 'Ingresa un número celular'
      },
      company: {
        required: 'Ingresa nombre de la empresa'
      },
      position: {
        required: 'Ingresa tu cargo'
      },
      voucher: {
          required: 'Ingresa tu imagen'
      }
    },
    errorPlacement: function(error, element) {
      var elem = $(element);
      if(elem.hasClass('jcf-real-element')){
          elem.parent().find('.jcf-fake-input').removeClass('input-good').addClass('input-error');
      }else {
          elem.removeClass('input-good').addClass('input-error');
          elem.prev().removeClass('icon-good').addClass('icon-error');
      }
    },
    unhighlight: function(element) {
      var elem = $(element);
      if(elem.hasClass('jcf-real-element')){
        elem.parent().find('.jcf-fake-input').removeClass('input-error').addClass('input-good');

      }else {
        elem.removeClass('input-error').addClass('input-good');
        elem.prev().removeClass('icon-error').addClass('icon-good');
      }
    }
  });
}

function saveForm() {
    $('#submit').click(function () {
        if($("#participant").valid()){
            var form_data = new FormData($('#participant')[0]);
            $.ajax({
                type: 'POST',
                url: '/api/participant',
                data: form_data,
                headers: {
                  'X-CSRFToken': csrftoken
                },
                processData: false,
                contentType: false,
                success: function(data) {
                    Swal.fire({
                      type: 'success',
                      title: 'Gracias por registrarte',
                      text: 'Un asesor se comunicará contigo en el transcurso del día'
                    });
                    $('.content-input').find('.icon').removeClass('icon-good');
                    $('#participant')[0].reset();
                }
            }).fail(function (data) {
                Swal.fire({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Ingrese los datos correctos'
                })

            });
        }else {
        }
    });

    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i].trim();
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
          }
        }
      }
      return cookieValue;
    }
    csrftoken = getCookie('csrftoken');
}
